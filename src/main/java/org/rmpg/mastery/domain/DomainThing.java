package org.rmpg.mastery.domain;

public class DomainThing
{
    private String origin;

    public DomainThing(String origin)
    {
        this.origin = origin;
    }

    public String getOrigin()
    {
        return origin;
    }
}
