package org.rmpg.mastery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = { "org.rmpg.mastery" })
public class SpringBootMainApplication
{
    public static void main(String[] args) // NOPMD - use the cannonical signature
    {
        SpringApplication.run(SpringBootMainApplication.class, args);
    }
}
